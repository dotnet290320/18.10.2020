﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class MySpecialData<T> 
    {
        private List<T> data;

        public T GetT(int index)
        {
            return data[index];
        }

        public T this[int index]
        {
            get
            {
                return data[index];
            }
        }

        public T this[string st1]
        {
            get
            {
                //return data[index];
                return data[0];
            }
        }

    }
}
