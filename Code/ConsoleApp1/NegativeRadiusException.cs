﻿using System;
using System.Runtime.Serialization;

namespace ConsoleApp1
{
    [Serializable]
    internal class NegativeRadiusException : Exception
    {
        public NegativeRadiusException()
        {
        }

        public NegativeRadiusException(string message) : base(message)
        {
        }

        public NegativeRadiusException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NegativeRadiusException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}