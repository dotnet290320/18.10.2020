﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Account
    {
        private double m_balance;
        public string CustomerName { get; set; }

        List<double> trans = new List<double>();

        public double Balance
        {
            get
            {
                return m_balance;
            }
        }
        public void Add(double add)
        {
            this.Add(add);
            trans.Add(add);
        }

        // acc1 = acc1 + 500;
        // acc2 = acc1 + 500;
        public static Account operator+(Account acc, double add)
        {
            // 90%
            //acc.m_balance += add;
            //return acc;

            // 100%
            Account clone = new Account(acc.Balance + add);
            acc.trans.Add(add);
            return clone;
        }

        public double this[int index]
        {
            get
            {
                return this.trans[index];
            }
            set //(int index, double value)
                //  a1[5] = 900;
            {
                this.trans[index] = value;
            }
        }

        public Account(double balance)
        {
            m_balance = balance;
            trans.Add(balance);
        }
    }
    class VipAccount : Account
    {
        public VipAccount(double balance) : base(balance)
        {
        }
    }
}
