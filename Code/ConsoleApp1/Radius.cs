﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using THREADS = System.Threading.Tasks;

namespace ConsoleApp1
{

    class CircleCompareByCreationDate : IComparer<Circle>
    {
        public int Compare(Circle x, Circle y)
        {
            return x.CreatedTime.CompareTo(y.CreatedTime);
        }
    }

    class Circle : IComparable<Circle>
    {
        private double m_radius;
        //public double m_radius; // BAD!!!!!!

        public double Radius { get; set; }
        public readonly DateTime CreatedTime = DateTime.Now;
        public double GetRadius()
        {
            return m_radius;
        }
        private void SetRadius(double value)
        {
            //if (value > 0)
            //{
                m_radius = value;
            //}
        }

        static int number_of_circles = 0;

        public const double Pie = 3.14;
        static Circle()
        {
            number_of_circles = 0; // read from file
            // access Db first time ...
            // initialization .................
        }

        public static int GetNumberOfcircles()
        {
            // Console.WriteLine(m_radius);
            return number_of_circles;
        }

        private readonly int number = 1;
        public const int MAX = 1000;
        public Circle(double radius)
        {
            if (radius < 0)
                throw new NegativeRadiusException($"cannot create a circle with negative radius. given value: {radius}");

            try
            {
                // 
                // boom
                // 1111
            }
            catch (ArgumentNullException ex)
            {
                //
            }
            finally
            {
                // release file
            }
            //
            number = 1;
            m_radius = radius;
            number_of_circles++;
        }
        public void Foo()
        {
            int x = 1000;
        }

        public int CompareTo(Circle other)
        {
            // this 
            // other
            // + 0 - 
            return this.Radius.CompareTo(other.Radius);

            if (this.Radius > other.Radius)
                return 1;
            else if (this.Radius == other.Radius)
                return 0;
            return -1;
        }
    }
}
