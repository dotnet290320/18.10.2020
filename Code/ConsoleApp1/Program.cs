﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void IncreaseBalance(Account acc)
        {
            acc = acc + 500;
        }
        // MOO
        // FOO
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter password:");
                if (Console.ReadLine() == "ABCD")
                    break;
            }
            Console.WriteLine("You made it!!! you won $1,000,000");

            int num = Circle.GetNumberOfcircles();

            // stack - push, pop, peek, clear -- LIFO
            //       3   2   1
            //           2   1
            //               1
            // list - random access, add/insert/remove random //Id:182673
            // dictionary - key/value {key:value} -- HashCode Equals {ID[182673]:Customer}
            // queue - Q --> LILO
            //      5 4 3  2  1 
            //       4 3 2 1 5
            //     
            List<Circle> list_of_circles = new List<Circle>();
            list_of_circles.Sort(); // IComparable
            list_of_circles.Sort(new CircleCompareByCreationDate()); // IComparer

            int a = 0;
            int b = 1;
            //int c = b / a;
            Account acc1 = new Account(123354);
            Account acc2 = new Account(345346);
            VipAccount vip = new VipAccount(34456);
            vip = (vip + 400) as VipAccount;
            acc1 = acc1 + 500;
            acc2 = acc1 + 900;
            //if (acc1!=acc2)
            //acc.Add(500);
            //acc == 500;
            Account a1 = new Account(900000);
            Account a2 = a1;
            IncreaseBalance(a1);
            Console.WriteLine(a1.Balance);
            Console.WriteLine(a1[40]);
            a1[5] = 900;
        }
    }
}
